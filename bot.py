import discord
from discord.ext import commands
from bot_token import TOKEN
import time
import os
import logging

client = commands.Bot(command_prefix = '>')

'''Events V'''

@client.event
async def on_ready():
    await client.change_presence(activity=discord.Game('Utility Bot'), status=discord.Status.idle, afk=False)
    print('Bot start up done!')
    print('Connected to discord!')

@client.event
async def on_member_remove(member):
    print(f'{member} has left the server')

@client.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument):
        await ctx.send('Please pass in all required arguments')


'''Commands V'''

@client.command()
async def ping(ctx):
    await ctx.send(f':ping_pong: Ponging...\n\n Bots Ping is: {round(client.latency * 1000)}ms')

@client.command()
async def clear(ctx, amount=5):
    await ctx.channel.purge(limit=amount)

@client.command()
async def load(ctx, extension):
    client.load_extension(f'cogs.{extension}')

@client.command()
async def unload(ctx, extension):
    client.unload_extension(f'cogs.{extension}')
            
print('Connecting to Discord...')

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

client.run(TOKEN)
