import discord
from discord.ext import commands
import pydactyl
from pydactyl import PterodactylClient
from ptero_keys import PteroClient

PteroClient = PterodactylClient('Domain Here!', 'Token Here!')

serverList = PteroClient.client.list_servers()

class panelapi(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.__last_member = None

    @commands.command()
    async def apipower(self, ctx, identifier, action):
        await ctx.send(f'Request sent to {action} a server with the ID of: {identifier}')
        PteroClient.client.send_power_action(identifier, action)

    @commands.command()
    async def serverstatus(self, ctx, identifier):
        util = PteroClient.client.get_server_utilization(identifier)

        if util['state'] == 'on':
            finalState = 'Started'
        
        if util['state'] == 'off':
            finalState = 'Offline'

        if util['state'] == 'restarting':
            finalState = 'Restarting'
        embedVar = discord.Embed(title="Server Status", description=f"Status Of: {identifier}I", color=0x00ff00)
        embedVar.add_field(name="State: ", value=finalState, inline=True)
        await ctx.send(embed=embedVar)








def setup(client):
    client.add_cog(panelapi(client))
